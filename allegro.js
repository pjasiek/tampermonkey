// ==UserScript==
// @name         allegro search by price
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://allegro.pl/listing?*
// @match        https://allegro.pl/kategoria*
// ==/UserScript==
(function() {
    'use strict';
    let href = new URL(window.location.href);

    if(href.searchParams.has('order') && 'p' !== href.searchParams.get('order')){
        href.searchParams.set('order', 'p');
        window.location.href = href;
    }
})();